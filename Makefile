CXX = g++
CXXFLAGS = -g
LD = g++
BUILDDIR = build
DEPDIR = $(BUILDDIR)/d
OBJDIR = $(BUILDDIR)/o
VPATH = src

SRC := $(wildcard $(VPATH)/*.cpp)
OBJ := $(patsubst $(VPATH)/%.cpp,$(OBJDIR)/%.o,$(SRC))

# .PHONY: info
# info:
# 	; $(info $$var is [${SRC}])echo Hello world
#
# m: $(OBJDIR)/main.o
# 	$(LD) -o $(BUILDDIR)/$@ $^

main: $(OBJ)
	$(LD) -o $(BUILDDIR)/$@ $^

.PHONY: run
run: main
	$(BUILDDIR)/main

.PHONY: clean
clean:
	-@rd /S /Q $(BUILDDIR)

$(OBJDIR)/%.o : %.cpp
	-@mkdir $(subst /,\,$(DEPDIR)) $(subst /,\,$(OBJDIR))
	$(CXX) $(CXXFLAGS) -c $< -o $@
	$(CXX) $(CXXFLAGS) -MM -MT $@ $< -o $(DEPDIR)/$(notdir $(basename $<).d)

-include $($(OBJDIR)/%.o=$(DEPDIR)/%.d)
