#ifndef COMPTE_EPARGNE_H
#define COMPTE_EPARGNE_H

#include "compte.h"

class CompteEpargne : public Compte{
private:
  double tauxInteret;
public:
  CompteEpargne(double soldeInitial, double tauxInteret);
  /** retourne le montant de l'interet gagne par le compte */
  double calculateInterest();
}; 
#endif
