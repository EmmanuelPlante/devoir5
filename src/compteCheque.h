#ifndef COMPTE_CHEQUE_H
#define COMPTE_CHEQUE_H

#include "compte.h"

class CompteCheque : public Compte{
private:
  double fraisTransaction;
public:
  CompteCheque(double soldeInitial, double fraisTransaction);
  void credit(double amount);
  bool debit(double amount);
}; 
#endif
