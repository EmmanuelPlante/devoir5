#ifndef COMPTE_H
#define COMPTE_H
/** classe de base pour un compte de banque **/
class Compte{
protected:
  /**le solde du compte**/
  double solde;
public:
  /** constructeur avec solde initial **/
  Compte(double soldeInitial);
  /** ajouter de l'argent au compte **/
  virtual void credit(double amount);
  /** retirer de l'argent
    *@return false s'il n'y a pas assez d'argent dans le compte
    */
  virtual bool debit(double amount);
  /** retourne le solde actuel du compte**/
  double getBalance();
};

#endif 
