#include "compteCheque.h"

CompteCheque::CompteCheque(double soldeInitial, double frais)
  : Compte(soldeInitial), fraisTransaction(frais)
{}

void CompteCheque::credit(double amount){
    Compte::credit(amount - CompteCheque::fraisTransaction);
}

bool CompteCheque::debit(double amount){
  bool ok = Compte::debit(amount + CompteCheque::fraisTransaction);
  //Autre possibilite si on considere que les frais peuvent amener le solde sous 0:
  /*
  bool ok = Compte::debit(amount);
  if(ok) solde-=fraisTransaction;
  */
  return ok;
} 
