#include "compteEpargne.h"

CompteEpargne::CompteEpargne(double soldeInitial, double tauxInteret)
  : Compte(soldeInitial), tauxInteret(tauxInteret)
{}

double CompteEpargne::calculateInterest(){
  return getBalance()*(tauxInteret/100.0); 
}
