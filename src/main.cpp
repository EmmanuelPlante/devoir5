#include "compte.h"
#include "compteCheque.h"
#include "compteEpargne.h"
#include <iostream>
#include <string>
#include <vector>
#include <typeinfo>

using namespace std;

int main() {

	//init comptes
	vector<Compte*> vecCom{ new CompteEpargne(500.0, 1), new CompteCheque(600.0, 5), new CompteCheque(400.0, 2), new CompteEpargne(100000.0, 2) };
	//pour la lecture
	double input;
	for (Compte* current : vecCom) {
		//juste pour savoir de quel compte on parle
		static int index=0;
		index++;
		//trouver le type de compte. le resultat depend du compilateur, mais ca devrait etre suffisant pour l'afficher
		auto type = typeid(*current).name();
		cout << "Account #"<<index<<": "<<type<<". balance: $"<<current->getBalance() <<endl<<"How much money to withdraw from the account?" <<endl;
		cin >> input;
		//checks if its actually a number (not needed if we assume only numbers are entered. also, if a invalid number is inputed, we could keep the input open until we get a good one, but i dont really care)44
		while (cin.fail()) {
			//clear error flag
			cin.clear();
			//ignore la ligne qui cause le probleme
			cin.ignore(256, '\n');
			cout << "Not a valid input! How much money to withdraw from the account?"<<endl;
			cin >> input;
		}
		//sort l'argent du compte
		current->debit(input);

		cout << "How much money to deposit to the account?"<<endl;
		cin >> input;
		//checks if its actually a number
		while (cin.fail()) {
			cin.clear();
			cin.ignore(256, '\n');
			cout << "Not a valid input! How much money to deposit to the account?"<<endl;
			cin >> input;
		}
		//ajoute l'argent au compte
		current->credit(input);

		//pour un compte epargne, ajoute l'interet. le retour de dynamic_cast est NULL (=false) si ce n'est pas le bon type de compte.
		CompteEpargne* downcasted = dynamic_cast<CompteEpargne*>(current);
		if (downcasted) {
			double interest=downcasted->calculateInterest();
			downcasted->credit(interest);
			cout << "Interest added: "<<interest<<endl;
		}
		//affiche le solde final
		cout << "The final balance is: $" << current->getBalance() << endl << endl;
	}
	//pour avoir le temps de lire la sortie...
	string term;
	cout << endl << "write exit to stop the program"<<endl;
	do {
		cin >> term;
	} while (term != "exit");

	return 0;
}
