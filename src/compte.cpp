#include "compte.h"
#include <iostream>
Compte::Compte(double soldeInitial)
  : solde(soldeInitial)
{
  if(solde<0){
    solde=0;
    std::cout << "Le solde initial doit etre positif!"<<std::endl;
  }
}

void Compte::credit(double amount){
  if(amount>0) solde+=amount;
  else std::cout << "Un credit doit etre superieur a 0!"<<std::endl;
}

bool Compte::debit(double amount){
  bool ok=amount<=solde;
  if(ok) solde-=amount;
  else std::cout << "Debit amount exceeded account balance"<<std::endl;
  return ok;
}

double Compte::getBalance(){
  return solde;
}
 